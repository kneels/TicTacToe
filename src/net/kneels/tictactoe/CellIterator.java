package net.kneels.tictactoe;

import java.util.Iterator;

public class CellIterator implements Iterator<Cell> 
{
	private Cell[][] grid;
	private int cursor = 0;
	private int maxIndex;

	public CellIterator(Cell[][] grid)
	{
		this.grid = grid;
		maxIndex = (grid.length * grid.length);
	}
	
	@Override
	public boolean hasNext() {		
		return (cursor < maxIndex) ? true : false;		
	}

	@Override
	public Cell next() {
		int x = cursor % grid.length;
		int y = cursor / grid.length;
		cursor++;
		return grid[x][y];
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("remove() method is not supported");
	}

}
