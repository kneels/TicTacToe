package net.kneels.tictactoe;

public enum GameResult {
	UNDECIDED, DRAW, HUMAN_WIN, AI_WIN
}
