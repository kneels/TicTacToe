package net.kneels.tictactoe;

public class AIPlayer extends Player {

	public AIPlayer(Grid grid, Mark playerMark) {
		super(grid, playerMark);
	}
	
	@Override
	public void makeMove() {
		Move bestMove = minimax(5, true);
		grid.doMove(bestMove, playerMark);
	}
	
	private Move minimax(int depth, boolean maximizingPlayer)
	{
		Move bestMove = new Move(-1, -1);
		Mark pMark = (maximizingPlayer) ? GameActivity.aiPlayerMark : GameActivity.humanPlayerMark; // AI is O
		
		if(grid.isGameOver() || depth == 0)
		{
			bestMove.setScore(grid.evaluateGameState());
			return bestMove;
		}
		
		if(maximizingPlayer)
		{
			bestMove.setScore(Integer.MIN_VALUE); 
			for(Move move : grid.getPossibleMoves())
			{
				grid.tryMove(move, pMark);
				move.setScore(minimax(depth - 1, false).getScore());
				grid.undoMove(move);
				if(move.getScore() > bestMove.getScore())
				{
					bestMove = move;
				}
			}
			return bestMove;
		}
		else
		{
			bestMove.setScore(Integer.MAX_VALUE); 
			for(Move move : grid.getPossibleMoves())
			{
				grid.tryMove(move, pMark);
				move.setScore(minimax(depth - 1, true).getScore());
				grid.undoMove(move);
				if(move.getScore() < bestMove.getScore())
				{
					bestMove = move;
				}
			}
			return bestMove;
		}
	}

	
}
