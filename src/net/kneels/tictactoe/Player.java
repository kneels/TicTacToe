package net.kneels.tictactoe;

public abstract class Player {
	
	protected Mark playerMark;
	protected Grid grid;
	
	public Player(Grid grid, Mark playerMark) {
		this.grid = grid;
		this.playerMark = playerMark;
	}
	
	public abstract void makeMove();	
		
	public Mark getPlayerMark()
	{
		return playerMark;
	}

}
