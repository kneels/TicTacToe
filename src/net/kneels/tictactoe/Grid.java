package net.kneels.tictactoe;

import java.util.ArrayList;
import java.util.Iterator;

public class Grid implements Iterable<Cell> {

	public final static int GRID_SIZE = 3;
	
	private Cell[][] grid;
	private GameActivity game;
	private GameResult gameResult = GameResult.UNDECIDED;
	
	public Grid(GameActivity game) {
		this.game = game;
		grid = new Cell[GRID_SIZE][GRID_SIZE];
		
		for(int y = 0; y < GRID_SIZE; y++)
		{
			for(int x = 0; x < GRID_SIZE; x++)
			{
				grid[x][y] = new Cell(x, y);
			}
		}
		
	}
	
	/**
	 * Checks a cell in the grid.
	 * @param move The position (aka Move) to be marked
	 * @param playerMark The Mark of the player (X, O)
	 */
	public void doMove(Move move, Mark playerMark) {
		int x = move.getX();
		int y = move.getY();
		
		if(grid[x][y].isEmpty())
		{
			grid[x][y].markCell(playerMark);
			game.moveMade();			
		}
	}
	
	/**
	 * Does the same as doMove except it does not notify
	 * the game activity.
	 */
	public void tryMove(Move move, Mark playerMark) {
		int x = move.getX();
		int y = move.getY();
		
		if(grid[x][y].isEmpty())
		{
			grid[x][y].markCell(playerMark);	
		}
	}
	
	public void undoMove(Move move)
	{
		int x = move.getX();
		int y = move.getY();
		grid[x][y].markCell(Mark.EMPTY);
	}
	
	/**
	 * Checks the 8 possible lines for a winner.
	 * Return true if there is a winner or the game is a draw.
	 */
	public boolean isGameOver()
	{
		int humRowSum = 0;
		int aiRowSum = 0;
		int humColSum = 0;
		int aiColSum = 0;
		int humDiagSum = 0;
		int aiDiagSum = 0;
		int humAntiDiagSum = 0;
		int aiAntiDiagSum = 0;
		
		
		for(int i = 0; i < GRID_SIZE; i++)
		{
			for(int k = 0; k < GRID_SIZE; k++)
			{
				if(grid[k][i].getMark() == GameActivity.humanPlayerMark)				
					humRowSum++;				
				else if(grid[k][i].getMark() == GameActivity.aiPlayerMark)
					aiRowSum++;
				if(grid[i][k].getMark() == GameActivity.humanPlayerMark)
					humColSum++;
				else if(grid[i][k].getMark() == GameActivity.aiPlayerMark)
					aiColSum++;
			}
			
			if(humRowSum == GRID_SIZE || humColSum == GRID_SIZE)
			{
				gameResult = GameResult.HUMAN_WIN;
				return true;
			}
			else if(aiRowSum == GRID_SIZE || aiColSum == GRID_SIZE)
			{
				gameResult = GameResult.AI_WIN;
				return true;
			}
			
			humRowSum = 0;
			aiRowSum = 0;
			humColSum = 0;
			aiColSum = 0;
		}
		
		int k = GRID_SIZE - 1;
		for(int i = 0; i < GRID_SIZE; i++)
		{
			if(grid[i][i].getMark() == GameActivity.humanPlayerMark)				
				humDiagSum++;				
			else if(grid[i][i].getMark() == GameActivity.aiPlayerMark)
				aiDiagSum++;
			
			if(grid[i][k].getMark() == GameActivity.humanPlayerMark)				
				humAntiDiagSum++;				
			else if(grid[i][k].getMark() == GameActivity.aiPlayerMark)
				aiAntiDiagSum++;
			
			k--;
		}
		if(humDiagSum == GRID_SIZE || humAntiDiagSum == GRID_SIZE)
		{
			gameResult = GameResult.HUMAN_WIN;
			return true;
		}
		else if(aiDiagSum == GRID_SIZE || aiAntiDiagSum == GRID_SIZE)
		{
			gameResult = GameResult.AI_WIN;
			return true;
		}
		
		if(getPossibleMoves().isEmpty())
		{
			gameResult = GameResult.DRAW;
			return true;
		}
		
		return false;
	}
	
	public ArrayList<Move> getPossibleMoves()
	{
		ArrayList<Move> possibleMoves = new ArrayList<Move>();
		for(int y = 0; y < GRID_SIZE; y++)
		{
			for(int x = 0; x < GRID_SIZE; x++)
			{
				if(grid[x][y].isEmpty())
					possibleMoves.add(new Move(x, y));
			}
		}
		return possibleMoves;
	}
	
	/**
	 * Evaluates the game state. The heuristic used is
	 * the number of the maximizing player's marks per
	 * line (a line being one of the 8 possible winning formations)
	 * minus the number of opponent's marks per line. 
	 * @return a positive integer for a favourable,
	 * a 0 for neutral or draw and negative for an unfavourable game state.	  
	 */
	public int evaluateGameState() {
		if(isGameOver())
		{
			switch(getGameResult())
			{
			case HUMAN_WIN:
				return Integer.MIN_VALUE;
			case AI_WIN:
				return Integer.MAX_VALUE;
			case DRAW:
				return 0;
			default:
				break;
			}
		}
		
		Mark myMark = GameActivity.aiPlayerMark;
		Mark oppMark = GameActivity.humanPlayerMark;
		int myLineSum = 0;
		int oppLineSum = 0;
		
		
		for(Cell[] row : getRows())
		{
			for(int i = 0; i < row.length; i++)
			{
				if(row[i].getMark() == myMark)
					myLineSum++;
				else if(row[i].getMark() == oppMark)
					oppLineSum++;
			}
		}
	
		for(Cell[] col : getColumns())
		{
			for(int i = 0; i < col.length; i++)
			{
				if(col[i].getMark() == myMark)
					myLineSum++;
				else if(col[i].getMark() == oppMark)
					oppLineSum++;
			}
		}
		
		for(Cell[] diag : getDiagonals())
		{
			for(int i = 0; i < diag.length; i++)
			{
				if(diag[i].getMark() == myMark)
					myLineSum++;
				else if(diag[i].getMark() == oppMark)
					oppLineSum++;
			}
		}
		
		return myLineSum - oppLineSum;
	}
	
	public ArrayList<Cell[]> getRows()
	{
		ArrayList<Cell[]> rows = new ArrayList<Cell[]>();
		
		for(int i = 0; i < GRID_SIZE; i++)
		{
			rows.add(new Cell[] { grid[0][i], grid[1][i], grid[2][i] });
		}
		
		return rows;
	}
	
	public ArrayList<Cell[]> getColumns()
	{
		ArrayList<Cell[]> cols = new ArrayList<Cell[]>();
		
		for(int i = 0; i < GRID_SIZE; i++)
		{
			cols.add(new Cell[] { grid[i][0], grid[i][1], grid[i][2] });
		}
		
		return cols;
	}
	
	public ArrayList<Cell[]> getDiagonals()
	{
		ArrayList<Cell[]> diags = new ArrayList<Cell[]>();
				
		// diagonal
		diags.add(new Cell[] { grid[0][0], grid[1][1], grid[2][2] });
		// anti-diagonal
		diags.add(new Cell[] { grid[0][2], grid[1][1], grid[2][0] });
				
		return diags;
	}

	@Override
	public Iterator<Cell> iterator() {
		return new CellIterator(grid);
	}

	public GameResult getGameResult() {
		return gameResult;
	}
	
}

