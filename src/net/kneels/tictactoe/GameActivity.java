package net.kneels.tictactoe;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

public class GameActivity extends Activity {
	
	private enum Turn { HUMAN, AI };
	private Turn turn = Turn.HUMAN;
	private GridLayout gridLayout;
	private TextView txtViewMessage;
	private String txtMessage;
	private Grid grid;
	private AIPlayer aiPlayer;
	private boolean gameOver;
	public static Mark humanPlayerMark = Mark.X;
	public static Mark aiPlayerMark = Mark.O;

    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        gridLayout = (GridLayout) findViewById(R.id.gridLayout);
        txtViewMessage = (TextView) findViewById(R.id.txtVIewMessage);
        grid = new Grid(this);
        aiPlayer = new AIPlayer(grid, aiPlayerMark);
        
        setupButtonGrid();
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.activity_main);
        }
        else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
        	
            setContentView(R.layout.activity_main);
        }
        gridLayout = (GridLayout) findViewById(R.id.gridLayout);
        txtViewMessage = (TextView) findViewById(R.id.txtVIewMessage);
        setupButtonGrid();
        txtViewMessage.setText(txtMessage);
    }

    /**
     * Notify the game that a move has been made. 
     * If the game is not over, let's the AI make a move. 
     */
	public void moveMade() 
	{		
		update();
		turn = (turn == Turn.HUMAN) ? Turn.AI : Turn.HUMAN;
		gameOver = grid.isGameOver();
		if(gameOver)
		{
			findWinner();
		}
		else if(turn == Turn.AI)
		{
			aiPlayer.makeMove();			
		}		
	}
	
	private void findWinner() 
	{		
		switch(grid.getGameResult())
		{
			case DRAW:
				txtMessage = getResources().getString(R.string.draw);
				break;
			case HUMAN_WIN:
				txtMessage = getResources().getString(R.string.human_win);
				break;
			case AI_WIN:
				txtMessage = getResources().getString(R.string.ai_win);
				break;
			default:
				txtMessage = "";
				break;
		}
		txtViewMessage.setText(txtMessage);
	}

	/**
	 * Creates a button view for every cell in the grid and
	 * adds them to the GridLayout.
	 */
	private void setupButtonGrid()
	{		
		for(Cell c : grid)
		{
			Button btnCell = c.toButton(this, c.toString());
			btnCell.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) 
				{
					if(!gameOver)
					{
						Button thisBtn = (Button) v;
						int[] cords = (int[]) thisBtn.getTag();	
						grid.doMove(new Move(cords[0], cords[1]), Mark.X);						
					}
				}
			});
			gridLayout.addView(btnCell);
		}
	}
	
	/**
	 * Updates the button views to represent the state of the grid model
	 */
	public void update()
	{
		int i = 0;
		for(Cell c : grid)
		{
			Button b = (Button) gridLayout.getChildAt(i++);
			b.setText(c.toString());
		}
		txtViewMessage.setText(txtMessage);
	}
	
	/**
	 * Resets the game parameters to their starting state
	 */
	public void restart(View v)
	{
		for(Cell c : grid)
		{
			c.unMarkCell();
		}
		txtMessage = "";
		turn = Turn.HUMAN;
		gameOver = false;
		update();
	}

}
