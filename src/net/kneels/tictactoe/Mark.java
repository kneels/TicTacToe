package net.kneels.tictactoe;

public enum Mark {
	EMPTY, 
	X, 
	O;
}
