package net.kneels.tictactoe;

import android.content.Context;
import android.graphics.Typeface;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;

public class Cell {
	
	private Mark mark;
	private int xPos, yPos;

	public Cell(int x, int y) {
		mark = Mark.EMPTY;
		xPos = x;
		yPos = y;
	}
	
	public void markCell(Mark mark)
	{
		this.mark = mark;
	}
	
	public void unMarkCell()
	{
		this.mark = Mark.EMPTY;
	}
	
	public boolean isEmpty()
	{
		return (mark == Mark.EMPTY) ? true : false; 
	}
	
	public Button toButton(Context context, String text)
	{
		Button btnCell = new Button(context);
		Typeface customTypeface = 
				Typeface.createFromAsset(context.getAssets(), "fonts/handwritingfont.ttf");
		btnCell.setTypeface(customTypeface);
		btnCell.setBackgroundResource(R.drawable.cell);
		btnCell.setHeight((int) context.getResources().getDimension(R.dimen.cell_dim));
		btnCell.setWidth((int) context.getResources().getDimension(R.dimen.cell_dim));
		btnCell.setTextSize(context.getResources().getDimension(R.dimen.cell_font_size));
		btnCell.setIncludeFontPadding(false);
		btnCell.setLayoutParams(new LayoutParams(
				LayoutParams.WRAP_CONTENT, 
				LayoutParams.WRAP_CONTENT));
		btnCell.setPadding(0, 0, 0, 0);
		btnCell.setText(text);
		btnCell.setTag(new int[] { xPos, yPos });		
		
		return btnCell;
	}
	
	@Override
	public String toString()
	{
		return (mark != Mark.EMPTY) ? mark.toString() : ""; 
	}
	
	@Override
	public boolean equals(Object other)
	{
		if(other == null) return false;			
		if(other == this) return true;
		Cell c = (Cell) other;
		if(c.getMark() == mark)
		{
			return true;
		}
		return false;
	}

	public Mark getMark() {
		return mark;
	}

}
