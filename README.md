TicTacToe
=========

An Android TicTacToe game with an AI player that uses the minimax algorithm, making it unbeatable.

(Android dev course week 2 assignment) 

![alt text](http://i.imgur.com/IEkwqVt.png)
